
const FIRST_NAME = "POPA";
const LAST_NAME = "RARES";
const GRUPA = "1090";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache={};
   cache.about=0;
   cache.home=0;
   cache.contact=0;
   cache.pageAccessCounter=function(page){
      console.log("The type of the parameter: "+ typeof(page));
      console.log(page);
      if(page!==undefined){
          if(page.toUpperCase()=="ABOUT"){
              this.about++;
          }else if(page.toUpperCase=="HOME"){
              this.home++;
          }else if(page.toUpperCase()=="CONTACT"){
              this.contact++;
          }}else{
              this.home++;
          }
      };
    cache.getCache=function(){
        return cache;
    }
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

